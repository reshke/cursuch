#pragma comment(linker, "/STACK:33554432")
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <queue>
#include <unordered_map>
#include <cmath>
#include <iterator>
#include <stack>
#include <unordered_set>
#include <iomanip>
#include <ctime>
#include <cstring>

using namespace std;

using ll = long long;
using ld = long double;

const ll MAXK = 26 * 2;
const ll STO_PITSOT = 100500;

const int MAXM = 21;
const int MAXGROUPCITIES = 12;
const int INF = 1e9;

int MAXCAPACITY;
int n;
int m;

int dist[MAXM][MAXM];
struct order
{
	int index;
	int city_number;
	int mass;
	order() {}
	order(int ind, int mass, int city_number)
	{
		index = ind;
		this->mass = mass;
		this->city_number = city_number;
	}
};

struct group
{
	vector<order> vertices;
	int total_mass = 0;

	int internal_bound = 0;
	set<int> cities;

	void add_vertex(order & ord)
	{
		for (auto el : this->vertices)
			internal_bound += dist[ord.index][el.index];

		this->vertices.push_back(ord);
		total_mass += ord.mass;
		this->cities.insert(ord.city_number);
	}

	bool is_empty() const
	{
		return this->vertices.empty();
	}
};

vector<order> orders;

const int MASKSIZE = 1 << (MAXGROUPCITIES + 1);

int dp[MASKSIZE][1 + MAXGROUPCITIES];
int par[MASKSIZE][1 + MAXGROUPCITIES];

void read_data()
{
	cin >> m >> n >> MAXCAPACITY;

	for (size_t i = 0; i <= m; ++i)
		for (size_t j = 0; j <= m; ++j)
			cin >> dist[i][j];

	for (int i = 0; i < n; ++i)
	{
		int a, b;
		cin >> a >> b;
		order ord = order(i + 1, a, b);
		orders.push_back(ord);
	}
}

bool check_strong_add_reason(const group& current_group, const order& current_order)
{
	return current_group.is_empty()
		|| current_group.cities.count(current_order.city_number) && current_group.total_mass + current_order.mass <= MAXCAPACITY;
}

bool sure_no_add(const group& current_group, const order& current_order)
{
	if (current_group.total_mass + current_order.mass > MAXCAPACITY)
		return true;

	if (current_group.cities.size() > MAXGROUPCITIES)
		return true;

	return false;
}


int check_vertex_to_add(const group& current_group, const order& current_order)
{
	int addition_dist = dist[0][current_order.city_number];

	for (auto each : current_group.vertices)
	{
		addition_dist = min(addition_dist, dist[each.city_number][current_order.city_number]);
	}

	return addition_dist;
}

vector<group> group_nodes()
{
	vector<group> ret;

	for (auto ord : orders)
	{
		bool was = false;
		for (int i = 0; i < ret.size(); ++i)
		{
			if (check_strong_add_reason(ret[i], ord))
			{
				was = true;
				ret[i].add_vertex(ord);
				break;
			}
		}

		if (was)
			continue;

		int group_min_value = INF;
		int group_min_index = -1;

		for (int i = 0; i < ret.size(); ++i)
		{
			if (sure_no_add(ret[i], ord))
				continue;
			auto val = check_vertex_to_add(ret[i], ord);

			if (val < group_min_value)
			{
				group_min_index = i;
				group_min_value = val;
			}
		}

		if (group_min_index == -1 || ret[group_min_index].internal_bound * 2 + 2000 < group_min_value)
		{
			auto prev = group();
			prev.add_vertex(ord);
			ret.push_back(prev);
		}
		else
		{
			ret[group_min_index].add_vertex(ord);
		}
	}

	return ret;
}

vector<int> solve_tsp_exact(vector<int>& initial)
{
	int curr_mask_size = 0;

	for (int i = 0; i <= initial.size(); ++i)
	{
		curr_mask_size |= 1 << i;
	}

	for (int i = 0; i <= curr_mask_size; ++i)
	{
		for (int j = 0; j <= initial.size(); ++j)
		{
			dp[i][j] = INF;
			par[i][j] = -1;
		}
	}

	dp[1][0] = 0;

	for (int i = 0; i <= curr_mask_size; ++i)
	{
		for (int j = 0; j <= initial.size(); ++j)
		{
			if ((i & (1 << j)) == 0)
				continue;


			for (int k = 1; k <= initial.size(); ++k)
			{
				if (k == j)
					continue;

				int curr_dist = 0;

				if (j == 0)
				{
					curr_dist = dist[0][initial[k - 1]];
				}
				else
				{
					curr_dist = dist[initial[j - 1]][initial[k - 1]];
				}

				if (dp[i][j] + curr_dist < dp[i | (1 << k)][k])
				{
					dp[i | (1 << k)][k] = dp[i][j] + dist[j][k];
					par[i | (1 << k)][k] = j;
				}
			}
		}
	}

	int mask = curr_mask_size;
	int end = 0;

	for (int i = 1; i <= initial.size(); ++i)
	{
		if (dp[mask][i] < dp[mask][end])
			end = i;
	}

	vector<int> answr;

	while (end != 0)
	{
		answr.push_back(initial[end - 1]);

		auto new_end = par[mask][end];
		mask ^= 1 << end;
		end = new_end;
	}

	reverse(answr.begin(), answr.end());

	return answr;
}

group solve_tsp(group current_group)
{

	vector<int> to_visit;
	for (auto el : current_group.cities)
		to_visit.push_back(el);

	group answr;

	for (auto curr : solve_tsp_exact(to_visit))
	{
		for (auto ord : current_group.vertices)
		{
			if (ord.city_number == curr)
				answr.add_vertex(ord);
		}
	}

	return answr;
}

void print_answr(vector<group> answr)
{
	cout << answr.size() << "\n\n";

	int tot_sum = 0;
	for (auto group : answr)
	{
		int sum = 0;
		for (auto el : group.vertices)
		{
			cout << el.index << " ";
			sum += el.mass;
		}
		cout << "\n";
		cout << sum << "\n";

		cout << "0 ";
		int sum_dist = 0;
		int prev = 0;
		for (auto el : group.vertices)
		{
			sum_dist += dist[prev][el.city_number];

			if (prev != el.city_number)
				cout << el.city_number << " ";

			prev = el.city_number;
		}
		sum_dist += dist[prev][0];
		tot_sum += sum_dist;

		cout << "0\n";
		cout << sum_dist << "\n\n";
	}

	cout << tot_sum;
}

void solve_dummy()
{
	auto groups = group_nodes();

	vector<group> answr;

	for (auto group : groups)
	{
		answr.push_back(solve_tsp(group));
	}

	print_answr(answr);
}

void solve()
{
	solve_dummy();
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(0), cout.tie(0);
	cout << fixed << setprecision(20);
#ifndef ONLINE_JUDGE
	freopen("input.in", "r", stdin);
	freopen("output.out", "w", stdout);
#endif
	read_data();
	solve();
#ifdef LOCAL 
	//cout << '\n' << (double)clock() / CLOCKS_PER_SEC << '\n';
#endif
}